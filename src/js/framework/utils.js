export function objectToMap(obj) {
    let map = new Map();

    for(let prop in obj) {
        if(obj.hasOwnProperty(prop)) {
            map.set(prop, obj[prop]);
        }
    }

    return map;
}
