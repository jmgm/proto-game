const listener = Symbol();

export default class BaseEventSource {
    constructor() {
        if(this.constructor === BaseEventSource) {
            throw new Error('BaseEventSource is an abstract class and cannot be instantiated.');
        }
    }

    emit(event, ...data) {
        this[listener](event, ...data);
    }

    listen(handler) {
        this[listener] = handler;
    }
}
