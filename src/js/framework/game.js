import { objectToMap } from './utils';
import { injectionMap, eventListeners } from './symbols';

const UPDATE_STEP = 10;

export default class Game {
    constructor({ models, eventSources, controllers, views, renderer }) {
        this.eventHandlers = new Map();

        this._installModels(models);
        this._installEventSources(eventSources);
        this._installControllers(controllers);
        this._installViews(views);
        this._installRenderer(renderer);
    }

    emitEvent(event, ...data) {
        if(this.eventHandlers.has(event)) {
            this.eventHandlers.get(event)
                .forEach(handler => handler(...data));
        }
    }

    run() {
        this.lastTime = performance.now();
        this.pause = false;
        this.run = true;
        requestAnimationFrame(::this._frame);
    }

    pause() {
        this.pause = true;
    }

    unpause() {
        this.pause = false;
    }

    stop() {
        this.run = false;
    }

    _frame() {
        let dt = -this.lastTime + (this.lastTime = performance.now());

        if(!this.pause) {
            let timeLeft = dt,
                step;

            while(timeLeft > 0) {
                step = timeLeft >= UPDATE_STEP ? UPDATE_STEP : timeLeft;
                timeLeft -= step;
                this.controllers.forEach(ctrl => ctrl.update(step));
            }
        }

        this.renderer.beginDrawing();
        this.views.forEach(view => view.render(this.renderer));
        this.renderer.finishDrawing();

        if(this.run) {
            requestAnimationFrame(::this._frame);
        }
    }

    _installModels(models) {
        this.models = objectToMap(models);
    }

    _installEventSources(sources) {
        this.eventSources = new Map();

        for(let prop in sources) {
            if(sources.hasOwnProperty(prop)) {
                let src = sources[prop];
                src.listen(::this.emitEvent);
                this.eventSources.set(prop, src);
            }
        }
    }

    _installControllers(controllers) {
        this.controllers = new Map();

        for(let prop in controllers) {
            if(controllers.hasOwnProperty(prop)) {
                let ctrl = controllers[prop];
                this._performInjection(ctrl);
                this._registerListeners(ctrl);
                this.controllers.set(prop, ctrl);
            }
        }
    }

    _installViews(views) {
        this.views = new Map();

        for(let prop in views) {
            if(views.hasOwnProperty(prop)) {
                let view = views[prop];
                this._performInjection(view);
                this.views.set(prop, view);
            }
        }
    }

    _installRenderer(renderer) {
        this.renderer = renderer;
    }

    _performInjection(obj) {
        if(obj[injectionMap]) {
            obj[injectionMap].forEach((model, prop) => {
                if(this.models.has(model)) {
                    obj[prop] = this.models.get(model);
                } else {
                    throw new Error(`Requested model "${model}" not found (${obj.constructor.name} injection).`);
                }
            });
        }
    }

    _registerListeners(ctrl) {
        if(ctrl[eventListeners]) {
            ctrl[eventListeners].forEach(([event, method]) => {
                if(this.eventHandlers.has(event)) {
                    this.eventHandlers.get(event).push(::ctrl[method]);
                } else {
                    this.eventHandlers.set(event, [::ctrl[method]]);
                }
            });
        }
    }
}
