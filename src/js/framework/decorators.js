import { objectToMap } from './utils';
import { injectionMap, eventListeners } from './symbols';

export function injectModels(map) {
    return target => {
        target.prototype[injectionMap] = objectToMap(map);
    };
}

export function on(event) {
    return (proto, method) => {
        (proto[eventListeners] || (proto[eventListeners] = []))
            .push([event, method]);
    };
}
