import { injectModels } from '../framework/decorators';

@injectModels({
    model: 'test'
})
export default class TestView {
    render(target) {
        target.drawDot(this.model.x, this.model.y);
    }
}
