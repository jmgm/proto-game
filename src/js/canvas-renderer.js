export default class CanvasRenderer {
    constructor({ canvas }) {
        this.ctx = canvas.getContext('2d');
    }

    beginDrawing() {
        this.ctx.clearRect(0, 0, 1000, 600);
    }

    finishDrawing() {}

    drawDot(x, y) {
        this.ctx.fillStyle = '#800';
        this.ctx.fillRect(x - 2, y - 2, 4, 4);
    }
}
