import { injectModels } from '../framework/decorators';

@injectModels({
    testModel: 'test'
})
export default class TestController {
    update(dt) {
        dt /= 1000;
        this.testModel.x += 50 * dt;
        this.testModel.y += 15 * dt;
    }
}
