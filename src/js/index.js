import Game from './framework/game';
import CanvasRenderer from './canvas-renderer';

import TestModel from './models/test-model';
import TestController from './controllers/test-controller';
import TestView from './views/test-view';

let testModel = new TestModel(),
    testController = new TestController(),
    testView = new TestView();

let game = new Game({
    models: {
        test: testModel
    },

    eventSources: {},

    controllers: {
        test: testController
    },

    views: {
        test: testView
    },

    renderer: new CanvasRenderer({
        canvas: document.getElementById('screen')
    })
});

game.run();
