var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    notifier = require('node-notifier'),
    path = require('path');

var errorHandler = function() {
    return plumber({
        errorHandler: function(err) {
            notifier.notify({
                title: err.plugin,
                message: err.message,
                icon: path.join(__dirname, 'gulp/icon.png')
            });
        }
    });
};

/*
    HTML
*/

var changed = require('gulp-changed');

gulp.task('index', function() {
    return gulp.src('src/index.html')
        .pipe(errorHandler())
        .pipe(changed('build/'))
        .pipe(gulp.dest('build/'));
});

/*
    JS
*/

var cache = require('gulp-cached'),
    babel = require('gulp-babel'),
    rename = require('gulp-rename'),
    webpack = require('gulp-webpack');

gulp.task('js-es6', function() {
    return gulp.src('src/js/**/*.js')
        .pipe(errorHandler())
        .pipe(cache('js-es6'))
        .pipe(babel({
            stage: 0
        }))
        .pipe(gulp.dest('.tmp/js/'));
});

gulp.task('js', ['js-es6'], function() {
    return gulp.src('.tmp/js/index.js')
        .pipe(errorHandler())
        .pipe(webpack())
        .pipe(rename('index.js'))
        .pipe(gulp.dest('build/'));
});

/*
    LESS
*/

var less = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer'),
    cleancss = require('gulp-minify-css');

gulp.task('less', function() {
    return gulp.src('src/less/index.less')
        .pipe(errorHandler())
        .pipe(less())
        .pipe(autoprefixer(['last 3 versions', '>= 1%']))
        .pipe(cleancss({
            keepSpecialComments: 0,
            keepBreaks: false,
            benchmark: false,
            processImport: false,
            noRebase: true,
            noAdvanced: false,
            compatibility: false,
            debug: false
        }))
        .pipe(gulp.dest('build/'));
});

/*
    Images
*/

gulp.task('imgs', function() {
    gulp.src(['src/imgs/**/*.png', 'src/imgs/**/*.jpg'])
        .pipe(errorHandler())
        .pipe(changed('build/imgs/'))
        .pipe(gulp.dest('build/imgs/'));
});

/*
    Fonts
*/

gulp.task('fonts', function() {
    gulp.src('src/fonts/fonts.less')
        .pipe(errorHandler())
        .pipe(less())
        .pipe(cleancss({
            keepSpecialComments: 0,
            keepBreaks: false,
            benchmark: false,
            processImport: false,
            noRebase: true,
            noAdvanced: false,
            compatibility: false,
            debug: false
        }))
        .pipe(gulp.dest('build/'));
});

/*
    General
*/

gulp.task('build', ['index', 'js', 'less', 'imgs', 'fonts']);

gulp.task('watch', function() {
    gulp.watch('src/index.html', ['index']);
    gulp.watch('src/js/**/*.js', ['js']);
    gulp.watch('src/less/**/*.less', ['less']);
    gulp.watch(['src/imgs/**/*.jpg', 'src/imgs/**/*.png'], ['imgs']);
    gulp.watch(['src/fonts/**/*.woff', 'src/fonts/fonts.less'], ['fonts']);
});

gulp.task('default', ['build', 'watch']);
