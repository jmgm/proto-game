# proto-game

## Architektura

### Kategorie obiektów

- główny obiekt gry
- dekoratory
- modele
- źródła eventów
- kontrolery
- widoki
- renderer

### Główny obiekt gry

Główny obiekt gry to instancja klasy `Game`.
Przy tworzeniu obiekt otrzymuje jako argumenty obiekty modeli, źródeł eventów,
kontrolerów, widoków i renderera, które zapisuje, jednocześnie wstrzykując
do nich zależności (modele) i rejestrując callbacki dla eventów.

Metoda `run` uruchamia główną pętlę gry. W każdym przebiegu pętli na wszystkich
kontrolerach odpalana jest metoda `update`, której argumentem jest ilość czasu,
jaki minął od ostatniego przebiegu. Kontrolery dokonują modyfikacji stanu modeli
w oparciu o różnicę czasu i ich aktualny stan.
Następnie uruchamiana jest metoda `render` wszystkich widoków po kolei.
Do metody przekazywany jest obiekt renderera. Widoki w oparciu o stan wybranych
modeli rysują odpowiednie elementy za pomocą metod renderera.

Pomiędzy przebiegami pętli następuje obsługa eventów. Źródła eventów wywołują
zdarzenia, których obsługą zajmują się kontrolery.

### Dekoratory

- `injectModels(map)` (dekorator klasy) - opisuje jakie modele mają zostać wstrzyknięte
do kontrolera / widoku (obiekt postaci `{ docelowe_pole: 'nazwa_modelu'... }`)
- `on(event)` (dekorator metody) - oznacza daną metodę jako callback eventu,
którego nazwa przekazana jest jako argument

### Modele

Standardowe obiekty JavaScript dowolnej postaci. Są wstrzykiwane do kontrolerów i widoków.

### Źródła eventów

Dziedziczą z klasy `BaseEventSource`, która implementuje metody `emit`
i `listen`. Metoda `emit` przyjmuje jako argumenty nazwę eventu i dodatkowe dane
i przekazuje je do handlera głównego obiektu gry, który następnie przekazuje
event kontrolerom zarejestrowanym jako słuchacze. Metoda `listen` przyjmuje
handler eventów głównego obiektu gry i zapisuje go do późniejszego użycia w `emit`.

### Kontrolery

*Podlegają wstrzykiwaniu modeli;
metody mogą być rejestrowane jako handlery eventów za pomocą dekoratora `on`.*

Muszą implementować metodę `update`.

### Widoki

*Podlegają wstrzykiwaniu modeli.*

Muszą implementować metodę `render`. Jako argument przekazywany jest do niej
obiekt renderera. Przy rysowaniu obiektów gry korzystają z danych z dostarczonych modeli.

### Renderer

Musi implementować ustalony zestaw metod (interfejs) pozwalających na rysowanie
obiektów gry (do ustalenia w przyszłości). Jest przekazywany do widoków
jako argument metody render.
